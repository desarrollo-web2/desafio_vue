var products = []

var elementVue = new Vue({
    el: '#table',
    data: {
        products: products
    },
    methods: {
        deleteProduct(index) {
            this.products.splice(index, 1)
        },
        updateProduct(index) {
            
            var name = getName()
            var desc = getDesc()
            var price = getPrice()

            if (name && desc && price) {

                var obj = {
                    name: name,
                    desc: desc,
                    price: price
                }

                this.products.splice(index, 1, obj)

            } else {
                alert("¡Rellena todos los campos!")
            }

        }
    }
})

$('#add_btn').click(() => {
    addProduct(getName(), getDesc(), getPrice())
    console.log(getName())
    console.log(getDesc())
    console.log(getPrice())
})

addProduct = (name, desc, price) => {

    if (name && desc && price) {
        
        var obj = {
            name: name,
            desc: desc,
            price: price
        }
    
        products.push(obj)

    } else {
        alert("¡Rellena todos los campos!")
    }

}

getName = () => {
    var str = $('#in_name').val()

    if (str.trim().length < 1) {
        return null
    } else {
        return str
    }
}

getDesc = () => {
    var str = $('#in_desc').val()

    return str
}

getPrice = () => {
    var str = $('#in_price').val()

    return str
}